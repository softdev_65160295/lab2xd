/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2xd;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2XD {

    static char Table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;

    public static void main(String[] args) {
        printTable();
        printTurn();
        inputRowCol();
        changePlayer();
        printTurn();

    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(Table[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input row,col : ");

        while (true) {
            row = kb.nextInt();
            col = kb.nextInt();
            if (Table[row - 1][col - 1]
                    == '-') {
                Table[row - 1][col - 1] = currentPlayer;
                break;
            }
        }
    }

    private static void changePlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

}
